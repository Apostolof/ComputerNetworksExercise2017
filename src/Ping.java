import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import ithakimodem.Modem;

public class Ping {
	private static final Scanner input = new Scanner(System.in);
	
	public static void ping() {
		System.out.print("Type request code: ");
		String requestCode = input.nextLine();
		System.out.print("Enter number of packages to request: ");
		int numberOfPackages = input.nextInt();
		input.nextLine(); //Clear input buffer
		
		pingServer(requestCode, numberOfPackages);
	}
	
	private static boolean pingServer(String requestCode, int numberOfPackages) {
		int[] timeDelayArray = new int[numberOfPackages];
		long startTime = 0, endTime = 0;
		
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("ping.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		}
		
		Modem modem = new Modem();
		modem.setSpeed(Utils.modemSpeed);
		modem.setTimeout(Utils.modemTimeout);
		modem.open(Utils.modemOpen);
		
		for(int i=0; i<numberOfPackages; ++i){
			String response = "";
			
			startTime = System.currentTimeMillis();
			response = Utils.makeRequest(modem, requestCode);
			endTime = System.currentTimeMillis();
			
			timeDelayArray[i] = (int)(endTime - startTime);
			writer.println(response + "\t" + timeDelayArray[i]);
		}
		modem.close();
		writer.close();
		
		return true;
	}
}