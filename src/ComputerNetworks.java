import java.util.Scanner;

public class ComputerNetworks {
	
	public static void main(String[] args) {
		final Scanner input = new Scanner(System.in);
		String functionChoosen;
		
		System.out.print("Available functions\n"
				+ "\t[1]: Ping ithaki server (ping)\n"
				+ "\t[2]: Get camera image with or without error (cam)\n"
				+ "\t[3]: Get GPS route image (gps)\n"
				+ "\t[4]: Simulate ARQ communication (arq)\n"
				+ "\t[0]: Exit (exit)\n"
				+ "To choose, type either the number in the brackets or"
				+ " the word in parentheses.\n");
		while(true) {
			System.out.print("\nSelect funtcion: ");
			functionChoosen = input.nextLine();
			if ("ping".equals(functionChoosen) || "1".equals(functionChoosen)) {
				Ping.ping();
			} else if ("cam".equals(functionChoosen) || "2".equals(functionChoosen)) {
				Camera.camera();
			} else if ("gps".equals(functionChoosen) || "3".equals(functionChoosen)) {
				GPS.gps();
			} else if ("arq".equals(functionChoosen) || "4".equals(functionChoosen)) {
				ACK.ack();
			} else if ("exit".equals(functionChoosen) || "0".equals(functionChoosen)) {
				break;
			} else {
				System.out.println("Wrong usage! Please be carefull of the syntax.");
			}
		}
		
		input.close();
	}
}
